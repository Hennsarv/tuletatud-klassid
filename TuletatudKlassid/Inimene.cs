﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TuletatudKlassid
{
    class Inimene
    {
        private static Dictionary<string, Inimene> _Inimesed = new Dictionary<string, Inimene>();
        public static List<Inimene> Nimekiri => _Inimesed.Values.ToList();


        private string _IK; public string IK => _IK;  
        public string Nimi;

        public Inimene(string ik, string nimi)
        {
            _IK = ik; Nimi = nimi;
            if (!_Inimesed.Keys.Contains(ik))
            _Inimesed.Add(_IK, this);
        }

        

        public override string ToString()
        {
            return $"{this.GetType().Name} {this.Nimi} ({this._IK})";
        }

    }
}
