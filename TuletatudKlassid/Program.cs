﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TuletatudKlassid
{
    class Program
    {
        static void Main(string[] args)
        {
            Õpetaja õ = new Õpetaja("1234", "Laur");
            Õpilane a = new Õpilane("1111", "Arno Tali");
            Lapsevanem l = new Lapsevanem("2222", "Arno isa");
            Inimene henn = new Inimene("3333", "Henn Sarv");

            new Õpetaja("999", "Mool", "keemia");

            Console.WriteLine("\nkõik inimesed:");

            foreach (var x in Inimene.Nimekiri)
            {
                Console.WriteLine(x);
            }

            Console.WriteLine("\nainult õpilased");

            foreach (var x in Õpilane.Nimekiri)
            {
                Console.WriteLine(x);
            }

            Console.WriteLine("\nAinult õpetajad");
            foreach(var x in Inimene.Nimekiri)
            {
                if (x is Õpetaja) Console.WriteLine(x);
            }
        }
    }
}
