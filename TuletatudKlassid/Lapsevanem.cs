﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TuletatudKlassid
{
    class Lapsevanem : Inimene
    {
        private static Dictionary<string, Lapsevanem> _Lapsevanemad = new Dictionary<string, Lapsevanem>();
        public new static  List<Inimene> Nimekiri => _Lapsevanemad.Values.OfType<Inimene>().ToList();

        public Lapsevanem(string ik, string nimi) : base(ik, nimi)
        {
            _Lapsevanemad.Add(ik, this);
        }

    }
}
