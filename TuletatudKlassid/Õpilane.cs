﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TuletatudKlassid
{
    class Õpilane : Inimene
    {
        private static Dictionary<string, Õpilane> _Õpilased = new Dictionary<string, Õpilane>();
        public new static  List<Inimene> Nimekiri => _Õpilased.Values.OfType<Inimene>().ToList();
        public Õpilane(string ik, string nimi) : base(ik, nimi)
        {
            _Õpilased.Add(ik, this);
        }
    }
}
