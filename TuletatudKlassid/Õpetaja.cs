﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TuletatudKlassid
{
    class Õpetaja : Inimene
    {
        private static Dictionary<string, Õpetaja> _Õpetajad = new Dictionary<string, Õpetaja>();

        public new static List<Inimene> Nimekiri => _Õpetajad.Values.OfType<Inimene>().ToList();

        public string Aine;


        public Õpetaja(string ik, string nimi, string aine = "") : base(ik, nimi)
        {
            this.Aine = aine;
            _Õpetajad.Add(ik, this);
        }

        public override string ToString()
        {
            return Aine == "" ? base.ToString() : $"{Aine} õpetaja {Nimi}" ;
        }
    }
}
